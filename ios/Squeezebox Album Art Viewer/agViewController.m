//
//  agViewController.m
//  Squeezebox Album Art Viewer
//
//  Created by andrej on 7/6/19.
//  Copyright (c) 2019 andrejg. All rights reserved.
//

#import "agViewController.h"
#import "TFHpple.h"
#import "UIImage+animatedGIF.h"
#import "AssetsLibrary/AssetsLibrary.h"

@interface agViewController ()

@end

@implementation agViewController
@synthesize img_view;

bool playing = false;
bool stopped = false;
bool paused = false;
bool is_radio = false;
bool is_connected = false;
NSString *song = nil;
int placeholder_change_sec = -1;
int prev_gif = -1;

NSString *status_path = @"/status.html?&player=";
NSString *cover_path = @"/music/current/cover.jpg?player=";
NSString *status_url = @"";
NSString *cover_url = @"";
NSString *server_url = NULL;
NSString *player_id = NULL;

NSString *script_address = @"https://example.com/img.php";
NSString *img_url = @"https://example.com/img/";

- (void)viewDidLoad
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    server_url = [defaults stringForKey:@"server_name"];
    player_id = [defaults stringForKey:@"player_id"];
        
    status_url = [NSString stringWithFormat:@"%@%@", server_url, [NSString stringWithFormat:@"%@%@", status_path, [player_id stringByReplacingOccurrencesOfString:@":" withString:@"%3A"]]];
    cover_url = [NSString stringWithFormat:@"%@%@", server_url, [NSString stringWithFormat:@"%@%@", cover_path, player_id]];
    
    NSTimer *check_connection = [NSTimer scheduledTimerWithTimeInterval:1
                                                                target:self
                                                              selector:@selector(check_connection)
                                                              userInfo:nil
                                                               repeats:YES];
    [self check_connection];
    
    // check status every 1 second
    NSTimer *check_status = [NSTimer scheduledTimerWithTimeInterval:1
                                                            target:self
                                                          selector:@selector(check_status)
                                                          userInfo:nil
                                                           repeats:YES];
    [self check_status];
    
    // refresh artwork every 1 second
    NSTimer *show_artwork = [NSTimer scheduledTimerWithTimeInterval:1
                                                            target:self
                                                          selector:@selector(show_artwork)
                                                          userInfo:nil
                                                           repeats:YES];
    [self show_artwork];
    
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)check_status {
    // get squeezebox status from status_url
    // parse out the status from <div id='playingStatus'>Now Playing/paused/stopped
    // the statuses are Playing (capital P!), stopped, paused
    // for our purposes paused equals stopped, i.e. we display an image and allow the device to enter sleep
    
    if (!is_connected) {
        // don't bother with anything if we're not connected
        return;
    }
    if (server_url == NULL) {
        // don't bother if server_url is not set in Settings (maybe display some warning?)
        return;
    }
    
    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:status_url]];
    
    TFHpple *doc = [[TFHpple alloc] initWithHTMLData:data];
    NSArray *playing_elements = [doc searchWithXPathQuery:@"//div[@id='playingStatus']"];
    TFHppleElement *element = [playing_elements objectAtIndex:0];
    NSString *status = [element text];
    
    NSRange pl = [status rangeOfString:@"Playing"]; // mind the capital P!
    NSRange st = [status rangeOfString:@"stopped"];
    NSRange pa = [status rangeOfString:@"paused"];
    
    if (pl.location == NSNotFound) {
        // not playing
    }
    else {
        playing = true;
        stopped = false;
        paused = false;
    }
    
    if (st.location == NSNotFound) {
        // not stopped
    }
    else {
        playing = false;
        stopped = true;
        paused = false;
    }
    
    if (pa.location == NSNotFound) {
        // not paused
    }
    else {
        playing = false;
        stopped = false;
        paused = true;
    }
    
    if (playing) {
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:status_url]];
        TFHpple *doc = [[TFHpple alloc] initWithHTMLData:data];
        NSArray *elements = [doc searchWithXPathQuery:@"//div[@class='playingSong']//a"];
        TFHppleElement *element = [elements objectAtIndex:0];
        
        NSString *song_info = [element objectForKey:@"href"];
        
        NSRange si = [song_info rangeOfString:@"item=-"]; // radio stations (or rather, streaming from an url) generally have a negative item id. The actual number varies. Perhaps from LMS install to LMS install, maybe from version to version.
        
        if (si.location == NSNotFound) {
            is_radio = false;
        }
        else {
            is_radio = true;
        }
    }
}

- (void)show_artwork {
    if (!is_connected) {
        return;
    }
    
    if (playing) {
        // prevent sleep
        [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
        
        if (!is_radio) {
            // get current title from status_url
            // the title is located in <div class='playingSong'><a href='...'>{title}</a>...</div>
            // we store the current song in song and compare it to current_song
            // if they're different - grab the artwork from cover_url and display it
            
            NSData *d = [NSData dataWithContentsOfURL:[NSURL URLWithString:status_url]];
            
            TFHpple *doc = [[TFHpple alloc] initWithHTMLData:d];
            NSArray *elements_title = [doc searchWithXPathQuery:@"//div[@class='playingSong']//a"];
            TFHppleElement *element_title = [elements_title objectAtIndex:0];
            NSString *current_song = [element_title text];
            if (![song isEqualToString:current_song]) {
                img_view.image = NULL;
                UIImage *i = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:cover_url]]];
                [UIView transitionWithView:self.img_view
                                  duration:1.0f
                                   options:UIViewAnimationOptionTransitionCrossDissolve
                                animations:^{
                                    self.img_view.image = i;
                                } completion:nil];
                img_view.contentMode = UIViewContentModeScaleAspectFill;
                song = current_song;
                placeholder_change_sec = -1;
            }
        }
        else {
            // if a radio station is playing we clear the previously playing song
            // and change the placeholder image every 180 seconds
            // if 180 seconds haven't elapsed yet we just increment placeholder_change_sec by one
            // timing is not really important here.
            
            song = nil;
            if (placeholder_change_sec == 180 || placeholder_change_sec == -1) {
                [self display_animated_gif];
                placeholder_change_sec = 0;
            }
            else {
                placeholder_change_sec++;
            }
        }
    }
    else {
        // same as with radio, except we change the placeholder every 300s
        
        song = nil;
        if (placeholder_change_sec == 300 || placeholder_change_sec == -1) {
            [self display_animated_gif];
            placeholder_change_sec = 0;
        }
        else {
            placeholder_change_sec++;
        }
    }
}

- (void)display_animated_gif {
    if (!is_connected) {
        // we _really_ need internet for this :)
        return;
    }
    
    // calls a simple php script that reads the contents of a directory and returns a random filename
    // we then load the file into gif and display it
    // no error checking since the files should exist and be accessible :)
    NSError *err;
    NSString *gif_url = [NSURL URLWithString:script_address];
    NSString *gif = [[NSString alloc]
                     initWithContentsOfURL:gif_url
                     encoding:NSUTF8StringEncoding
                     error:&err];
    gif = [NSString stringWithFormat:[img_url stringByAppendingString:@"%@"], gif]; // modify the php script to return a full url?
    UIImage *i = [UIImage animatedImageWithAnimatedGIFURL:[NSURL URLWithString:gif]];
    [UIView transitionWithView:self.img_view
                      duration:1.0f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        self.img_view.image = i;
                    }completion:nil];
    img_view.contentMode = UIViewContentModeScaleAspectFill;
}

- (void)check_connection {
    // check if we're connected to the internet and display an image if not.
    Reachability *r = [Reachability reachabilityForInternetConnection];
    NetworkStatus s = [r currentReachabilityStatus];
    UIImage *i = [UIImage imageNamed:@"not-connected.jpg"];
    if (s != NotReachable) {
        is_connected = true;
    }
    else {
        img_view.image = NULL;
        [UIView transitionWithView:self.img_view
                          duration:1.0f
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            self.img_view = i;
                        } completion:nil];
        is_connected = false;
    }
}

@end

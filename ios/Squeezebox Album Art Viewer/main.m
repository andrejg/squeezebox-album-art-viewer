//
//  main.m
//  Squeezebox Album Art Viewer
//
//  Created by andrej on 7/6/19.
//  Copyright (c) 2019 andrejg. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "agAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([agAppDelegate class]));
    }
}

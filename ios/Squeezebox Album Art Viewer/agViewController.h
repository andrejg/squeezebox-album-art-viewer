//
//  agViewController.h
//  Squeezebox Album Art Viewer
//
//  Created by andrej on 7/6/19.
//  Copyright (c) 2019 andrejg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import <SystemConfiguration/SystemConfiguration.h>

@interface agViewController : UIViewController
- (void) connected;
- (void) check_status;
- (void) show_artwork;
@property (weak, nonatomic) IBOutlet UIImageView *img_view;

@end

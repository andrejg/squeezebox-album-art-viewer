//
//  agAppDelegate.h
//  Squeezebox Album Art Viewer
//
//  Created by andrej on 7/6/19.
//  Copyright (c) 2019 andrejg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface agAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

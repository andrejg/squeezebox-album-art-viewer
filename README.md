# Logitech Squeezebox album art viewer

![image](img/photo.jpg "image")

![components](img/components.jpg "components")

Repurposing an old, useless, 1st gen iPad to be an album art viewer.

The app checks a given Logitech Media Server for the status of a given Squeezebox player. If it's playing anything it displays the song's cover. If it doesn't it grabs a random animated gif from a PHP script on another server and displays it, cycling through the gifs every 5 minutes. Same when streaming radio.

## Video:

[![video](https://img.youtube.com/vi/GoInjNy5wsY/0.jpg)](https://www.youtube.com/watch?v=GoInjNy5wsY)

### Todo
* LMS autodiscovery (http://wiki.slimdevices.com/index.php/SLIMP3_client_protocol)
* option for choosing the player for which the album art is displayed (from the players available on the LMS)